﻿using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 前置订单放行/退回接口响应
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    public class DeliveryNoticeResponse : BaseResponse
    {
    }
}
