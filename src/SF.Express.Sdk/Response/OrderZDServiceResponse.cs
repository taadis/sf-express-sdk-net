﻿using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 子单号申请接口响应
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    public class OrderZDServiceResponse : BaseResponse
    {
        [XmlElement(ElementName = "Body")]
        public OrderZDServiceResponseBody Body { get; set; }
    }

    public class OrderZDServiceResponseBody
    {
        [XmlElement(ElementName = "OrderZDResponse")]
        public OrderZDResponse OrderZDResponse { get; set; }
    }

    public class OrderZDResponse
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "main_mailno")]
        public string MainMailNo { get; set; }

        [XmlAttribute(AttributeName = "mailno_zd")]
        public string MailNoZd { get; set; }
    }
}
