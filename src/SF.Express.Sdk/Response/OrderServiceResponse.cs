﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 下订单接口响应
    /// </summary>
    [XmlRoot(elementName: "Response")]
    public class OrderServiceResponse : BaseResponse
    {
        [XmlElement(ElementName = "Body")]
        public OrderServiceResponseBody Body { get; set; }
    }

    public class OrderServiceResponseBody
    {
        [XmlElement(ElementName = "OrderResponse")]
        public OrderServiceResponseOrderResponse OrderResponse { get; set; }
    }

    public class OrderServiceResponseOrderResponse
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "mailno")]
        public string MailNo { get; set; }

        [XmlAttribute(AttributeName = "return_tracking_no")]
        public string ReturnTrackingNo { get; set; }

        [XmlAttribute(AttributeName = "origincode")]
        public string OriginCode { get; set; }

        [XmlAttribute(AttributeName = "destcode")]
        public string DestCode { get; set; }

        [XmlAttribute(AttributeName = "filter_result")]
        public string FilterResult { get; set; }

        [XmlAttribute(AttributeName = "remark")]
        public string Remark { get; set; }

        [XmlAttribute(AttributeName = "mapping_mark")]
        public string MappingMark { get; set; }

        [XmlAttribute(AttributeName = "isUpstairs")]
        public string IsUpstairs { get; set; }

        [XmlAttribute(AttributeName = "isSpecialWarehouseService")]
        public string IsSpecialWarehouseService { get; set; }

        [XmlElement(ElementName = "rls_info")]
        public RlsInfo RlsInfo { get; set; }
    }

    public class RlsInfo
    {
        [XmlAttribute(AttributeName = "invoke_result")]
        public string InvokeResult { get; set; }

        [XmlAttribute(AttributeName = "rls_code")]
        public string RlsCode { get; set; }

        [XmlAttribute(AttributeName = "errorDesc")]
        public string ErrorDesc { get; set; }

        [XmlElement(ElementName = "rls_detail")]
        public RlsDetail RlsDetail { get; set; }
    }

    public class RlsDetail
    {
        [XmlAttribute(AttributeName = "waybillNo")]
        public string WaybillNo { get; set; }

        [XmlAttribute(AttributeName = "sourceTransferCode")]
        public string SourceTransferCode { get; set; }

        [XmlAttribute(AttributeName = "sourceCityCode")]
        public string SourceCityCode { get; set; }

        [XmlAttribute(AttributeName = "sourceDeptCode")]
        public string SourceDeptCode { get; set; }

        [XmlAttribute(AttributeName = "sourceTeamCode")]
        public string SourceTeamCode { get; set; }

        [XmlAttribute(AttributeName = "destCityCode")]
        public string DestCityCode { get; set; }

        [XmlAttribute(AttributeName = "destDeptCode")]
        public string DestDeptCode { get; set; }

        [XmlAttribute(AttributeName = "destDeptCodeMapping")]
        public string DestDeptCodeMapping { get; set; }

        [XmlAttribute(AttributeName = "destTeamCode")]
        public string DestTeamCode { get; set; }

        [XmlAttribute(AttributeName = "destTeamCodeMapping")]
        public string DestTeamCodeMapping { get; set; }

        [XmlAttribute(AttributeName = "destTransferCode")]
        public string DestTransferCode { get; set; }

        [XmlAttribute(AttributeName = "destRouteLabel")]
        public string DestRouteLabel { get; set; }

        [XmlAttribute(AttributeName = "proName")]
        public string ProName { get; set; }

        [XmlAttribute(AttributeName = "cargoTypeCode")]
        public string CargoTypeCode { get; set; }

        [XmlAttribute(AttributeName = "limitTypeCode")]
        public string LimitTypeCode { get; set; }

        [XmlAttribute(AttributeName = "expressTypeCode")]
        public string ExpressTypeCode { get; set; }

        [XmlAttribute(AttributeName = "codingMapping")]
        public string CodingMapping { get; set; }

        [XmlAttribute(AttributeName = "codingMappingOut")]
        public string CodingMappingOut { get; set; }

        [XmlAttribute(AttributeName = "xbFlag")]
        public string XbFlag { get; set; }

        [XmlAttribute(AttributeName = "printFlag")]
        public string PrintFlag { get; set; }

        [XmlAttribute(AttributeName = "twoDimensionCode")]
        public string TwoDimensionCode { get; set; }

        [XmlAttribute(AttributeName = "proCode")]
        public string ProCode { get; set; }

        [XmlAttribute(AttributeName = "printIcon")]
        public string PrintIcon { get; set; }

        [XmlAttribute(AttributeName = "abFlag")]
        public string AbFlag { get; set; }

        [XmlAttribute(AttributeName = "errMsg")]
        public string ErrMsg { get; set; }

        [XmlAttribute(AttributeName = "destPortCode")]
        public string DestPortCode { get; set; }

        [XmlAttribute(AttributeName = "destCountry")]
        public string DestCountry { get; set; }

        [XmlAttribute(AttributeName = "destPostCode")]
        public string DestPostCode { get; set; }

        [XmlAttribute(AttributeName = "goodsValueTotal")]
        public string GoodsValueTotal { get; set; }

        [XmlAttribute(AttributeName = "currencySymbol")]
        public string CurrencySymbol { get; set; }

        [XmlAttribute(AttributeName = "goodsNumber")]
        public string GoodsNumber { get; set; }

        [XmlAttribute(AttributeName = "checkCode")]
        public string CheckCode { get; set; }
    }
}
