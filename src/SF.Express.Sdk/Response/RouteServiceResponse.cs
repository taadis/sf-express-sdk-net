﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 路由查询接口响应
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    public class RouteServiceResponse : BaseResponse
    {
        [XmlElement(ElementName = "Body")]
        public RouteServiceResponseBody Body { get; set; }
    }

    public class RouteServiceResponseBody
    {
        [XmlElement(ElementName = "RouteResponse")]
        public List<RouteResponse> RouteResponse { get; set; }
    }

    public class RouteResponse
    {
        [XmlAttribute(AttributeName = "mailno")]
        public string MailNo { get; set; }

        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlElement(ElementName = "Route")]
        public RouteResponseRoute Route { get; set; }
    }

    public class RouteResponseRoute
    {
        [XmlAttribute(AttributeName = "accept_time")]
        public string AcceptTime { get; set; }

        [XmlAttribute(AttributeName = "accept_address")]
        public string AcceptAddress { get; set; }

        [XmlAttribute(AttributeName = "remark")]
        public string Remark { get; set; }

        [XmlAttribute(AttributeName = "opcode")]
        public string OpCode { get; set; }
    }
}
