﻿using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 订单确认/取消接口响应
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    public class OrderConfirmServiceResponse : BaseResponse
    {
        [XmlElement(ElementName = "Body")]
        public OrderConfirmServiceResponseBody Body { get; set; }
    }

    public class OrderConfirmServiceResponseBody
    {
        [XmlElement(ElementName = "OrderConfirmResponse")]
        public OrderConfirmResponse OrderConfirmResponse { get; set; }
    }

    public class OrderConfirmResponse
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "mailno")]
        public string MailNo { get; set; }

        [XmlAttribute(AttributeName = "res_status")]
        public string ResStatus { get; set; }
    }
}
