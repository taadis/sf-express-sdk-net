﻿using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 订单筛选接口响应
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    public class OrderFilterServiceResponse : BaseResponse
    {
        [XmlElement(ElementName = "Body")]
        public OrderFilterServiceResponseBody Body { get; set; }
    }

    public class OrderFilterServiceResponseBody
    {
        [XmlElement(ElementName = "OrderFilterResponse")]
        public OrderFilterResponse OrderFilterResponse { get; set; }
    }

    public class OrderFilterResponse
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "filter_result")]
        public string FilterResult { get; set; }

        [XmlAttribute(AttributeName = "origincode")]
        public string OriginCode { get; set; }

        [XmlAttribute(AttributeName = "destcode")]
        public string DestCode { get; set; }

        [XmlAttribute(AttributeName = "remark")]
        public string Remark { get; set; }
    }
}
