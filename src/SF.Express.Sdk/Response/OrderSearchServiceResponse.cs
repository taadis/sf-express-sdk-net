﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 订单结果查询接口响应
    /// </summary>
    [XmlRoot(elementName: "Response")]
    public class OrderSearchServiceResponse : BaseResponse
    {
        [XmlElement(ElementName = "Body")]
        public OrderSearchServiceBody Body { get; set; }
    }

    public class OrderSearchServiceBody
    {
        [XmlElement(ElementName = "OrderResponse")]
        public OrderResponse OrderResponse { get; set; }
    }

    public class OrderResponse
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "mailno")]
        public string MailNo { get; set; }

        [XmlAttribute(AttributeName = "origincode")]
        public string OriginCode { get; set; }

        [XmlAttribute(AttributeName = "destcode")]
        public string DestCode { get; set; }

        [XmlAttribute(AttributeName = "filter_result")]
        public string FilterResult { get; set; }

        [XmlAttribute(AttributeName = "remark")]
        public string Remark { get; set; }
    }
}
