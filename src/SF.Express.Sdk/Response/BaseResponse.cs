﻿using System.Xml.Serialization;

namespace SF.Express.Sdk.Response
{
    /// <summary>
    /// 基础响应
    /// </summary>
    public abstract class BaseResponse
    {
        [XmlAttribute(attributeName: "service")]
        public string Service { get; set; }

        [XmlElement(elementName: "Head")]
        public string Head { get; set; }

        [XmlElement(elementName: "ERROR")]
        public Error Error { get; set; }

        // 各接口响应继承后自行扩展
    }

    public class Error
    {
        [XmlAttribute(attributeName: "code")]
        public string Code { get; set; }

        [XmlText]
        public string Message { get; set; }
    }
}
