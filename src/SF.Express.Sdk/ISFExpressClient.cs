﻿using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;

namespace SF.Express.Sdk
{
    public interface ISFExpressClient
    {
        T Execute<T>(BaseRequest<T> request) where T : BaseResponse;
    }
}
