﻿using SF.Express.Sdk.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 订单结果查询接口请求
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class OrderSearchServiceRequest : BaseRequest<OrderSearchServiceResponse>
    {
        public OrderSearchServiceRequest()
        {
            Service = "OrderSearchService";
        }

        [XmlElement(ElementName = "Body")]
        public OrderSearchBody Body { get; set; }
    }

    public class OrderSearchBody
    {
        [XmlElement(ElementName = "OrderSearch")]
        public OrderSearch OrderSearch { get; set; }
    }

    public class OrderSearch
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "search_type")]
        public string SearchType { get; set; }
    }
}
