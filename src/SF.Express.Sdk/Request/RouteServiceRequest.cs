﻿using SF.Express.Sdk.Response;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 路由查询接口请求
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class RouteServiceRequest : BaseRequest<RouteServiceResponse>
    {
        public RouteServiceRequest()
        {
            Service = "RouteService";
        }

        [XmlElement(ElementName = "Body")]
        public RouteServiceRequestBody Body { get; set; }
    }

    public class RouteServiceRequestBody
    {
        [XmlElement(ElementName = "RouteRequest")]
        public RouteRequest RouteRequest { get; set; }
    }

    public class RouteRequest
    {
        [XmlAttribute(AttributeName = "tracking_type")]
        public string TrackingType { get; set; }

        [XmlAttribute(AttributeName = "tracking_number")]
        public string TrackingNumber { get; set; }

        [XmlAttribute(AttributeName = "method_type")]
        public string MethodType { get; set; }

        [XmlAttribute(AttributeName = "reference_number")]
        public string ReferenceNumber { get; set; }

        [XmlAttribute(AttributeName = "check_phoneNo")]
        public string CheckPhoneNo { get; set; }
    }
}
