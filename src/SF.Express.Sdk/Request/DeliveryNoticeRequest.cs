﻿using SF.Express.Sdk.Response;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 前置订单放行/退回接口请求
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class DeliveryNoticeRequest : BaseRequest<DeliveryNoticeResponse>
    {
        public DeliveryNoticeRequest()
        {
            this.Service = "DeliveryNoticeService";
        }

        [XmlElement(ElementName = "Body")]
        public DeliveryNoticeRequestBody Body { get; set; }
    }

    public class DeliveryNoticeRequestBody
    {
        [XmlElement(ElementName = "WaybillNoValidate")]
        public WaybillNoValidate WaybillNoValidate { get; set; }
    }

    public class WaybillNoValidate
    {
        [XmlAttribute(AttributeName = "waybillNo")]
        public string WaybillNo { get; set; }

        [XmlAttribute(AttributeName = "dataType")]
        public string DataType { get; set; }
    }
}
