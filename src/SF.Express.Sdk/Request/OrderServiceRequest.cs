﻿using SF.Express.Sdk.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 下订单接口请求
    /// </summary>
    [XmlRoot(elementName: "Request")]
    public class OrderServiceRequest : BaseRequest<OrderServiceResponse>
    {
        public OrderServiceRequest()
        {
            Service = "OrderService";
        }

        [XmlElement(elementName: "Body")]
        public Body Body { get; set; }
    }

    public class Body
    {
        [XmlElement(elementName: "Order")]
        public Order Order { get; set; }
    }

    public class Order
    {
        [XmlAttribute("orderid")]
        public string OrderId { get; set; }

        [XmlAttribute("j_company")]
        public string JCompany { get; set; }

        [XmlAttribute("j_contact")]
        public string JContact { get; set; }

        [XmlAttribute("j_tel")]
        public string JTel { get; set; }

        [XmlAttribute(attributeName: "j_mobile")]
        public string JMobile { get; set; }

        [XmlAttribute(attributeName: "j_province")]
        public string JProvince { get; set; }

        [XmlAttribute(attributeName: "j_city")]
        public string JCity { get; set; }

        [XmlAttribute(attributeName: "j_county")]
        public string JCounty { get; set; }

        [XmlAttribute(attributeName: "j_address")]
        public string JAddress { get; set; }

        [XmlAttribute("d_company")]
        public string DCompany { get; set; }

        [XmlAttribute("d_contact")]
        public string DContact { get; set; }

        [XmlAttribute("d_tel")]
        public string DTel { get; set; }

        [XmlAttribute("d_mobile")]
        public string DMobile { get; set; }

        [XmlAttribute("d_province")]
        public string DProvince { get; set; }

        [XmlAttribute("d_city")]
        public string DCity { get; set; }

        [XmlAttribute("d_county")]
        public string DCounty { get; set; }

        [XmlAttribute("d_address")]
        public string DAddress { get; set; }

        [XmlAttribute("custid")]
        public string CustId { get; set; }

        [XmlAttribute("pay_method")]
        public string PayMethod { get; set; }

        [XmlAttribute(attributeName: "express_type")]
        public string ExpressType { get; set; }

        [XmlAttribute("parcel_quantity")]
        public string ParcelQuantity { get; set; }

        [XmlAttribute("cargo_length")]
        public string CargoLength { get; set; }

        [XmlAttribute("cargo_width")]
        public string CargoWidth { get; set; }

        [XmlAttribute("cargo_height")]
        public string CargoHeight { get; set; }

        [XmlAttribute("volume")]
        public string Volume { get; set; }

        [XmlAttribute(AttributeName = "cargo_total_weight")]
        public string CargoTotalWeight { get; set; }

        [XmlAttribute("sendstarttime")]
        public string SendStartTime { get; set; }

        [XmlAttribute("is_docall")]
        public string IsDocall { get; set; }

        [XmlAttribute("need_return_tracking_no")]
        public string NeedReturnTrackingNo { get; set; }

        [XmlAttribute("return_tracking")]
        public string ReturnTracking { get; set; }

        [XmlAttribute("temp_range")]
        public string TempRange { get; set; }

        [XmlAttribute("template")]
        public string Template { get; set; }

        [XmlAttribute("remark")]
        public string Remark { get; set; }

        [XmlAttribute("oneself_pickup_flg")]
        public string OneselfPickupFlg { get; set; }

        [XmlAttribute("special_delivery_type_code")]
        public string SpecialDeliveryTypeCode { get; set; }

        [XmlAttribute("special_delivery_value")]
        public string SpecialDeliveryValue { get; set; }

        [XmlAttribute("realname_num")]
        public string RealnameNum { get; set; }

        [XmlAttribute("routelabelForReturn")]
        public string RoutelabelForReturn { get; set; }

        [XmlAttribute("routelabelService")]
        public string RoutelabelService { get; set; }

        [XmlAttribute("is_unified_waybill_no")]
        public string IsUnifiedWaybillNo { get; set; }

        [XmlElement(elementName: "Cargo")]
        public Cargo Cargo { get; set; }

        [XmlElement(elementName: "AddedService")]
        public AddedService AddedService { get; set; }

        [XmlElement(elementName: "Extra")]
        public Extra Extra { get; set; }
    }

    public class Cargo
    {
        [XmlAttribute(attributeName: "name")]
        public string Name { get; set; }

        [XmlAttribute(attributeName: "count")]
        public string Count { get; set; }
    }

    public class AddedService
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("value1")]
        public string Value1 { get; set; }

        [XmlAttribute("value2")]
        public string Value2 { get; set; }

        [XmlAttribute("value3")]
        public string Value3 { get; set; }

        [XmlAttribute("value4")]
        public string Value4 { get; set; }
    }

    public class Extra
    {
        [XmlAttribute("e1")]
        public string E1 { get; set; }

        [XmlAttribute("e2")]
        public string E2 { get; set; }

        [XmlAttribute("e3")]
        public string E3 { get; set; }

        [XmlAttribute("e4")]
        public string E4 { get; set; }

        [XmlAttribute("e5")]
        public string E5 { get; set; }

        [XmlAttribute("e6")]
        public string E6 { get; set; }

        [XmlAttribute("e7")]
        public string E7 { get; set; }

        [XmlAttribute("e8")]
        public string E8 { get; set; }

        [XmlAttribute("e9")]
        public string E9 { get; set; }

        [XmlAttribute("e10")]
        public string E10 { get; set; }

        [XmlAttribute("e11")]
        public string E11 { get; set; }

        [XmlAttribute("e12")]
        public string E12 { get; set; }

        [XmlAttribute("e13")]
        public string E13 { get; set; }

        [XmlAttribute("e14")]
        public string E14 { get; set; }

        [XmlAttribute("e15")]
        public string E15 { get; set; }

        [XmlAttribute("e16")]
        public string E16 { get; set; }

        [XmlAttribute("e17")]
        public string E17 { get; set; }

        [XmlAttribute("e18")]
        public string E18 { get; set; }

        [XmlAttribute("e19")]
        public string E19 { get; set; }

        [XmlAttribute("e20")]
        public string E20 { get; set; }
    }
}
