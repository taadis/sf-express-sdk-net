﻿using SF.Express.Sdk.Response;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 基础请求
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseRequest<T> where T : BaseResponse
    {
        [XmlAttribute(attributeName: "service")]
        public string Service { get; set; }

        [XmlAttribute(attributeName: "lang")]
        public string Lang { get; set; }

        [XmlElement(elementName: "Head")]
        public string Head { get; set; }

        [XmlIgnore]
        public string CheckWord { get; set; }
    }
}
