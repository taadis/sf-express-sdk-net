﻿using SF.Express.Sdk.Response;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 子单号申请接口请求
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class OrderZDServiceRequest : BaseRequest<OrderZDServiceResponse>
    {
        public OrderZDServiceRequest()
        {
            Service = "OrderZDService";
        }

        [XmlElement(ElementName = "Body")]
        public OrderZDServiceRequestBody Body { get; set; }
    }

    public class OrderZDServiceRequestBody
    {
        [XmlElement(ElementName = "OrderZD")]
        public OrderZD OrderZD { get; set; }
    }

    public class OrderZD
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "parcel_quantity")]
        public int ParcelQuantity { get; set; }
    }
}
