﻿using SF.Express.Sdk.Response;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 订单确认/取消接口请求
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class OrderConfirmServiceRequest : BaseRequest<OrderConfirmServiceResponse>
    {
        public OrderConfirmServiceRequest()
        {
            Service = "OrderConfirmService";
        }

        [XmlElement(ElementName = "Body")]
        public OrderConfirmServiceRequestBody Body { get; set; }
    }

    public class OrderConfirmServiceRequestBody
    {
        [XmlElement(ElementName = "OrderConfirm")]
        public OrderConfirm OrderConfirm { get; set; }
    }

    public class OrderConfirm
    {
        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "mailno")]
        public string MailNo { get; set; }

        [XmlAttribute(AttributeName = "dealtype")]
        public string DealType { get; set; }

        [XmlAttribute(AttributeName = "customs_batchs")]
        public string CustomsBatchs { get; set; }

        [XmlAttribute(AttributeName = "agent_no")]
        public string AgentNo { get; set; }

        [XmlAttribute(AttributeName = "consign_emp_code")]
        public string ConsignEmpCode { get; set; }

        [XmlAttribute(AttributeName = "source_zone_code")]
        public string SourceZoneCode { get; set; }

        [XmlAttribute(AttributeName = "in_process_waybill_no")]
        public string InProcessWaybillNo { get; set; }
    }

    public class OrderConfirmOption
    {
        [XmlAttribute(AttributeName = "weight")]
        public string Weight { get; set; }

        [XmlAttribute(AttributeName = "volume")]
        public string Volume { get; set; }

        [XmlAttribute(AttributeName = "return_tracking")]
        public string ReturnTracking { get; set; }

        [XmlAttribute(AttributeName = "express_type")]
        public string ExpressType { get; set; }

        [XmlAttribute(AttributeName = "children_nos")]
        public string ChildrenNos { get; set; }

        [XmlAttribute(AttributeName = "waybill_size")]
        public string WaybillSize { get; set; }

        [XmlAttribute(AttributeName = "is_gen_eletric_pic")]
        public string IsGenEletricPic { get; set; }
    }
}
