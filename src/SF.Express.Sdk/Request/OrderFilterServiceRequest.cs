﻿using SF.Express.Sdk.Response;
using System.Xml.Serialization;

namespace SF.Express.Sdk.Request
{
    /// <summary>
    /// 订单筛选接口请求
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class OrderFilterServiceRequest : BaseRequest<OrderFilterServiceResponse>
    {
        public OrderFilterServiceRequest()
        {
            Service = "OrderFilterService";
        }

        [XmlElement(ElementName = "Body")]
        public OrderFilterServiceRequestBody Body { get; set; }
    }

    public class OrderFilterServiceRequestBody
    {
        [XmlElement(ElementName = "OrderFilter")]
        public OrderFilter OrderFilter { get; set; }
    }

    public class OrderFilter
    {
        [XmlAttribute(AttributeName = "filter_type")]
        public string FilterType { get; set; }

        [XmlAttribute(AttributeName = "orderid")]
        public string OrderId { get; set; }

        [XmlAttribute(AttributeName = "d_address")]
        public string DAddress { get; set; }

        [XmlElement(ElementName = "OrderFilterOption")]
        public OrderFilterOption OrderFilterOption { get; set; }
    }

    public class OrderFilterOption
    {
        [XmlAttribute(AttributeName = "j_tel")]
        public string JTel { get; set; }

        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }

        [XmlAttribute(AttributeName = "province")]
        public string Province { get; set; }

        [XmlAttribute(AttributeName = "city")]
        public string City { get; set; }

        [XmlAttribute(AttributeName = "county")]
        public string County { get; set; }

        [XmlAttribute(AttributeName = "d_country")]
        public string DCountry { get; set; }

        [XmlAttribute(AttributeName = "d_province")]
        public string DProvince { get; set; }

        [XmlAttribute(AttributeName = "d_city")]
        public string DCity { get; set; }

        [XmlAttribute(AttributeName = "d_county")]
        public string DCounty { get; set; }

        [XmlAttribute(AttributeName = "j_address")]
        public string JAddress { get; set; }

        [XmlAttribute(AttributeName = "d_tel")]
        public string DTel { get; set; }

        [XmlAttribute(AttributeName = "j_custid")]
        public string JCustId { get; set; }
    }
}
