﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SF.Express.Sdk.Helper
{
    public class HttpHelper
    {
        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典</param>
        /// <returns>URL编码后的请求数据</returns>
        public static string BuildQuery(IDictionary<string, string> parameters)
        {
            if (parameters == null) return null;
            if (!parameters.Any()) return null;
            StringBuilder query = new StringBuilder();
            bool hasParam = false;
            foreach (KeyValuePair<string, string> kv in parameters)
            {
                string name = kv.Key;
                string value = kv.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                    {
                        query.Append("&");
                    }

                    query.Append(value: name);
                    query.Append("=");
                    query.Append(value: HttpUtility.UrlEncode(value, Encoding.UTF8));
                    hasParam = true;
                }
            }
            return query.ToString();
        }
    }
}
