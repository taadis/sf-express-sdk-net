﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SF.Express.Sdk.Helper
{
    public class VerifyCodeHelper
    {
        private static byte[] md5Encrypt(string s)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(s: s);
            MD5 md5 = new MD5CryptoServiceProvider();
            return md5.ComputeHash(buffer: buffer);
        }

        private static string toBase64String(byte[] buffer)
        {
            return Convert.ToBase64String(inArray: buffer);
        }

        public static string Compute(string s)
        {
            return toBase64String(buffer: md5Encrypt(s: s));
        }
    }
}
