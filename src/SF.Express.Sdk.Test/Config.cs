﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SF.Express.Sdk.Test
{
    public class Config
    {
        /// <summary>
        /// 顾客编码
        /// </summary>
        public const string ClientCode = "";

        /// <summary>
        /// 校检码
        /// </summary>
        public const string CheckWord = "";
    }
}
