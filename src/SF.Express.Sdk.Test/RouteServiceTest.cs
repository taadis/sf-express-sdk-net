﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class RouteServiceTest
    {
        private readonly SFExpressClient _client;

        public RouteServiceTest()
        {
            _client = new SFExpressClient();
        }

        [TestMethod]
        public void RouteServiceTest1()
        {
            RouteServiceRequest routeServiceTest = new RouteServiceRequest();
            RouteServiceResponse routeServiceResponse = _client.Execute(request: routeServiceTest);
            Assert.AreEqual(expected: "ERR", actual: routeServiceResponse.Head);
            Assert.AreEqual(expected: "4001", actual: routeServiceResponse.Error.Code);
        }

        [TestMethod]
        public void RouteServiceTest2()
        {
            RouteServiceRequest routeServiceTest = new RouteServiceRequest();
            routeServiceTest.Lang = "zh-CN";
            routeServiceTest.Head = Config.ClientCode;
            routeServiceTest.CheckWord = Config.CheckWord;
            routeServiceTest.Body = new RouteServiceRequestBody
            {
                RouteRequest = new RouteRequest
                {
                    TrackingType = "2",
                    MethodType = "1",
                    TrackingNumber = "444003077898",
                    CheckPhoneNo = "1234"
                }
            };
            RouteServiceResponse routeServiceResponse = _client.Execute(request: routeServiceTest);
            Assert.AreEqual(expected: "OK", actual: routeServiceResponse.Head);
        }
    }
}
