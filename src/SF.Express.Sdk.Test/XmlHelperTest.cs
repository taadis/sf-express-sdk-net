﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Helper;
using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class XmlHelperTest
    {
        [TestMethod]
        public void Serialize1()
        {
            OrderServiceRequest o = new OrderServiceRequest();
            o.Lang = "zh-CN";
            o.Head = "顾客编码";
            string xmlString = XmlHelper.Serialize(o: o);
            Assert.IsNotNull(value: xmlString);
        }

        [TestMethod]
        public void Deserialize1()
        {
            OrderServiceRequest o = new OrderServiceRequest();
            o.Lang = "zh-CN";
            o.Head = "顾客编码";
            string xmlString = XmlHelper.Serialize(o: o);
            OrderServiceRequest oo = XmlHelper.Deserialize<OrderServiceRequest>(s: xmlString);
            Assert.AreEqual(expected: o.Service, actual: oo.Service);
            Assert.AreEqual(expected: o.Lang, actual: oo.Lang);
            Assert.AreEqual(expected: o.Head, actual: oo.Head);
        }

        [TestMethod]
        public void Deserialize2()
        {
            string xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Head>ERR</Head><ERROR code=\"传入格式错误,XML:\" /></Response>";
            OrderServiceResponse o = XmlHelper.Deserialize<OrderServiceResponse>(s: xmlString);
            Assert.AreEqual(expected: "ERR", actual: o.Head);
            Assert.AreEqual(expected: "传入格式错误,XML:", actual: o.Error.Code);
        }
    }
}
