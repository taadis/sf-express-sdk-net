﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class OrderZDServiceTest
    {
        private readonly SFExpressClient _client;

        public OrderZDServiceTest()
        {
            _client = new SFExpressClient();
        }

        [TestMethod]
        public void OrderZDServiceTest1()
        {
            OrderZDServiceRequest orderZDServiceRequest = new OrderZDServiceRequest();
            OrderZDServiceResponse orderZDServiceResponse = _client.Execute(request: orderZDServiceRequest);
            Assert.AreEqual(expected: "ERR", actual: orderZDServiceResponse.Head);
            Assert.AreEqual(expected: "4001", actual: orderZDServiceResponse.Error.Code);
        }

        [TestMethod]
        public void OrderZDServiceTest2()
        {
            OrderZDServiceRequest orderZDServiceRequest = new OrderZDServiceRequest();
            orderZDServiceRequest.Lang = "zh-CN";
            orderZDServiceRequest.Head = Config.ClientCode;
            orderZDServiceRequest.CheckWord = Config.CheckWord;
            orderZDServiceRequest.Body = new OrderZDServiceRequestBody
            {
                OrderZD = new OrderZD
                {
                    OrderId = "xxx",
                    ParcelQuantity = 2
                }
            };
            OrderZDServiceResponse orderZDServiceResponse = _client.Execute(request: orderZDServiceRequest);
            Assert.AreEqual(expected: "ERR", actual: orderZDServiceResponse.Head);
            Assert.AreEqual(expected: "8018", actual: orderZDServiceResponse.Error.Code);
        }
    }
}
