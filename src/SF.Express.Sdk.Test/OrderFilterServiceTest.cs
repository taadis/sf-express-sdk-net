﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class OrderFilterServiceTest
    {
        private readonly SFExpressClient _client;

        public OrderFilterServiceTest()
        {
            _client = new SFExpressClient();
        }

        [TestMethod]
        public void OrderFilterServiceTest1()
        {
            OrderFilterServiceRequest orderFilterServiceRequest = new OrderFilterServiceRequest();
            OrderFilterServiceResponse orderFilterServiceResponse = _client.Execute(request: orderFilterServiceRequest);
            Assert.AreEqual(expected: "ERR", actual: orderFilterServiceResponse.Head);
            Assert.AreEqual(expected: "4001", actual: orderFilterServiceResponse.Error.Code);
        }
    }
}
