﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class DeliveryNoticeServiceTest
    {
        private readonly SFExpressClient _client;

        public DeliveryNoticeServiceTest()
        {
            _client = new SFExpressClient();
        }

        [TestMethod]
        public void DeliveryNoticeServiceTest1()
        {
            DeliveryNoticeRequest request = new DeliveryNoticeRequest();
            DeliveryNoticeResponse response = _client.Execute(request: request);
            Assert.AreEqual(expected: "ERR", actual: response.Head);
            Assert.AreEqual(expected: "4001", actual: response.Error.Code); // 系统发生数据错误或运行时异常
        }

        [TestMethod]
        public void DeliveryNoticeServiceTest2()
        {
            DeliveryNoticeRequest request = new DeliveryNoticeRequest();
            request.Lang = "zh-CN";
            request.Head = Config.ClientCode;
            request.CheckWord = Config.CheckWord;
            request.Body = new DeliveryNoticeRequestBody();
            request.Body.WaybillNoValidate = new WaybillNoValidate();
            request.Body.WaybillNoValidate.WaybillNo = "1";
            request.Body.WaybillNoValidate.DataType = "2";
            DeliveryNoticeResponse response = _client.Execute(request: request);
            Assert.AreEqual(expected: "ERR", actual: response.Head);
            Assert.AreEqual(expected: "8026", actual: response.Error.Code); // 不存在的客户
        }

        [TestMethod]
        public void DeliveryNoticeServiceTest3()
        {
            DeliveryNoticeRequest request = new DeliveryNoticeRequest();
            request.Lang = "zh-CN";
            request.Head = Config.ClientCode;
            request.CheckWord = Config.CheckWord;
            request.Body = new DeliveryNoticeRequestBody();
            request.Body.WaybillNoValidate = new WaybillNoValidate();
            request.Body.WaybillNoValidate.WaybillNo = "1";
            request.Body.WaybillNoValidate.DataType = "2";
            DeliveryNoticeResponse response = _client.Execute(request: request);
            Assert.AreEqual(expected: "ERR", actual: response.Head);
            Assert.AreEqual(expected: "A1004", actual: response.Error.Code); // 无对应服务权限
            //Assert.AreEqual(expected: "DeliveryNoticeService", actual: response.Service);
        }
    }
}
