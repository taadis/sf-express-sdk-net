﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Helper;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class VerifyCodeHelperTest
    {
        [TestMethod]
        public void Compute1()
        {
            string verifyCode = VerifyCodeHelper.Compute(s: "123456");
            Assert.AreEqual(expected: "4QrcOUm6Wau+VuBX8g+IPg==", actual: verifyCode);
        }
    }
}
