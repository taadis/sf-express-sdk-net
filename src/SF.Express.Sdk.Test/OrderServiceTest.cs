﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SF.Express.Sdk.Request;
using SF.Express.Sdk.Response;
using System;

namespace SF.Express.Sdk.Test
{
    [TestClass]
    public class OrderServiceTest
    {
        private readonly SFExpressClient _client;

        public OrderServiceTest()
        {
            _client = new SFExpressClient();
        }

        [TestMethod]
        public void OrderServiceTest1()
        {
            OrderServiceRequest orderServiceRequest = new OrderServiceRequest();
            OrderServiceResponse orderServiceResponse = _client.Execute(request: orderServiceRequest);
            Assert.AreEqual(expected: "ERR", actual: orderServiceResponse.Head);
            Assert.AreEqual(expected: "4001", actual: orderServiceResponse.Error.Code);
        }

        [TestMethod]
        public void OrderServiceTest2()
        {
            OrderServiceRequest orderServiceRequest = new OrderServiceRequest();
            OrderServiceResponse orderServiceResponse = _client.Execute(request: orderServiceRequest);
            Assert.AreEqual(expected: "4001", actual: orderServiceResponse.Error.Code);
        }

        [TestMethod]
        public void OrderServiceTest3()
        {
            OrderServiceRequest orderServiceRequest = new OrderServiceRequest();
            orderServiceRequest.Lang = "zh-CN";
            orderServiceRequest.Head = Config.ClientCode;
            orderServiceRequest.CheckWord = Config.CheckWord;
            orderServiceRequest.Body = new Body
            {
                Order = new Order
                {
                    Cargo = new Cargo
                    {
                        Name = "手机"
                    },
                    AddedService = new AddedService
                    {
                        Name = "COD",
                        Value = "1.01",
                        Value1 = "7551234567"
                    },
                    Extra = new Extra { }
                }
            };
            orderServiceRequest.Body.Order.OrderId = DateTime.Now.ToString(format: "yyyyMMddHHmmssfff");
            orderServiceRequest.Body.Order.ExpressType = "1";
            orderServiceRequest.Body.Order.JProvince = "广东省";
            orderServiceRequest.Body.Order.JCity = "深圳市";
            orderServiceRequest.Body.Order.JCompany = "顺丰镖局";
            orderServiceRequest.Body.Order.JContact = "艾丽丝";
            orderServiceRequest.Body.Order.JTel = "15012345678";
            orderServiceRequest.Body.Order.JAddress = "福田区新洲十一街万基商务大厦26楼";
            orderServiceRequest.Body.Order.DProvince = "广东省";
            orderServiceRequest.Body.Order.DCity = "深圳市";
            orderServiceRequest.Body.Order.DCounty = "";
            orderServiceRequest.Body.Order.DCompany = "神罗科技";
            orderServiceRequest.Body.Order.DContact = "风一样的旭哥";
            orderServiceRequest.Body.Order.DTel = "33992159";
            orderServiceRequest.Body.Order.DAddress = "海珠区宝芝林大厦701室&#xA;6号";
            orderServiceRequest.Body.Order.ParcelQuantity = "1";
            orderServiceRequest.Body.Order.PayMethod = "3";
            orderServiceRequest.Body.Order.CustId = "7551234567";
            orderServiceRequest.Body.Order.JMobile = "15322234342";
            orderServiceRequest.Body.Order.JCounty = "南山区";
            orderServiceRequest.Body.Order.DMobile = "15423456545";
            orderServiceRequest.Body.Order.Remark = "电子产品 笔记本显卡";
            OrderServiceResponse orderServiceResponse = _client.Execute(request: orderServiceRequest);
            Assert.AreEqual(expected: "OK", actual: orderServiceResponse.Head);
        }
    }
}
