# sf-express-sdk-net

顺丰丰桥软件开发工具包(.NET)

## 相关

- [丰桥](https://qiao.sf-express.com/)

## 开发环境

- Visual Studio 2012
- 目标框架: `.NET Framework 4.0`

> 后续版本会出 .NET Core, 毕竟现在用 .NET Framework 还是蛮多的, 成年人嘛全都要...

## 安装

- [SF.Express.Sdk](https://www.nuget.org/packages/SF.Express.Sdk) - Nuget

## 版本发布

发布命令 `nuget pack SF.Express.Sdk.csproj`

## 单元测试

单元测试中包含完整的使用示例, 可供参考.
